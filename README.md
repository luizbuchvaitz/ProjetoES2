* Tecnologias Utilizadas

Linguagens 

#### Java
#### SQL

* Ferramentas Utilizadas

#### NetBeans IDE 8.2 -> https://netbeans.org
#### XAMPP -> https://www.apachefriends.org/pt_br/index.html
#### mysql-connector-java -> https://dev.mysql.com/downloads/connector/j/

* Como faço para ter acesso ao projeto?
* É bem simples, logo abaixo irá ter passo a passo de como você poderá acessar o projeto.

#### 1º Passo: 
#### Você irá clonar o projeto, usando o comando: git clone git@gitlab.com:luizbuchvaitz/ProjetoES2.git
![1º Passo](http://i.imgur.com/wVrHHuf.png)


#### 2º Passo:
#### Você precisará abrir o XAMPP e executar o MySQL, dentro do XAMPP irá selecionar a opção "Start" no Apache e MySQL e irá abrir a opção "admin" em MySQL,
#### logo após você irá ter que importar a tabela para poder acessar a tabela e visualizar os  usuários cadastrados, caixa de entrada e saída destes usuários.
![2º Passo](http://i63.tinypic.com/equrnd.png)
![2º Passo](http://i.imgur.com/D3XPpLA.png)

#### 3º Passo:
#### Por ultimo, irá ter que adicionar o plugin "mysql-connector-java" na biblioteca do projeto no NetBeans.
![3º Passo](http://i65.tinypic.com/25ixlw0.png)
