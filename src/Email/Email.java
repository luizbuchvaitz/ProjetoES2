package Email;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Email {

    private final int maxUsers = 5000;
    private int indiceUser;

    private Usuario[] usuarios = new Usuario[maxUsers];

    private Scanner entrada = new Scanner(System.in);

    public void menu() {
        int opcao;

        OUTER:
        while (true) {

            System.out.println("+------------------------------------+");
            System.out.println("|           Email Senac              |");
            System.out.println("+------------------------------------+");
            System.out.println("|                                    |");
            System.out.println("|.1 Cadastrar                        |");
            System.out.println("|.2 Logar                            |");
            System.out.println("|.3 Sair                             |");
            System.out.println("|                                    |");
            System.out.println("+------------------------------------+");
            System.out.print("|Digite a opção: ");

            try {
                opcao = entrada.nextInt();
            } catch (Exception e) {
                opcao = -1;
            }

            entrada.nextLine();

            switch (opcao) {
                case 1:
                    cadastrar();
                    break;
                case 2:
                    logar();
                    break;
                case 3:
                    break OUTER;
                default:
                    break;
            }

            try {
                System.in.read();
            } catch (Exception e) {
            }
        }
        System.out.println("");
    }

    private void cadastrar() {

        System.out.println("+------------------------------------+");
        System.out.println("|              Cadastrar             |");
        System.out.println("+------------------------------------+\n");
        System.out.print("|Nome: ");
        String nome = entrada.nextLine();
        System.out.print("|Email: ");
        String email = entrada.nextLine();
        System.out.print("|Senha: ");
        String senha = entrada.nextLine();
        System.out.print("|Telefone: ");
        String telefone = entrada.nextLine();

        boolean emailNaoExiste = true;

        for (int i = 0; i < indiceUser; i++) {
            if (usuarios[i].getEmail().equals(email)) {
                emailNaoExiste = false;
                break;
            }
        }

        if (emailNaoExiste) {
            if (indiceUser < maxUsers) {
                try {
                    Connection conexao = null;
                    PreparedStatement pst = null;
                    ResultSet rs = null;

                    conexao = ModuloConexao.conector();

                    String sql = "INSERT INTO usuario (nome, email, senha, telefone) VALUES (?,?,?,?);";

                    pst = conexao.prepareStatement(sql);
                    pst.setString(1, nome);
                    pst.setString(2, email);
                    pst.setString(3, senha);
                    pst.setString(4, telefone);
                    pst.executeUpdate();

                    conexao.close();

                    usuarios[indiceUser] = new Usuario(nome, email, senha, telefone);
                    entrar(usuarios[indiceUser]);
                    indiceUser++;
                } catch (SQLException ex) {
                    Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("\nErro! O número de usuários cadastrados chegou no limite!");
            }
        } else {
            System.out.println("\nErro! Este endereço de email já existe!");
        }
    }

    private void logar() {

        System.out.println("+--------------------------------+");
        System.out.println("|              Logar             |");
        System.out.println("+--------------------------------+\n");
        System.out.print("|Email: ");
        String email = entrada.nextLine();
        System.out.print("|Senha: ");
        String senha = entrada.nextLine();

        Usuario usuario = null;

        for (int i = 0; i < indiceUser; i++) {
            if (usuarios[i].getEmail().equals(email) && usuarios[i].getSenha().equals(senha)) {
                usuario = usuarios[i];
                break;
            }
        }

        if (usuario != null) {
            entrar(usuario);
        } else {
            System.out.println("\nErro! Usuário ou senha não encontrados!");
        }
    }

    private void entrar(Usuario usuario) {

        int opcao;

        OUTER:
        while (true) {

            System.out.println("+------------------------------------+");
            System.out.println("|         Seja bem-vindo(a)          |");
            System.out.println("+------------------------------------+\n");
            System.out.println("| " + usuario.getNome() + "\n");
            System.out.println("+------------------------------------+");
            System.out.println("|                                    |");
            System.out.println("|.1 Enviar email                     |");
            System.out.println("|.2 Visualizar perfil                |");
            System.out.println("|.3 Caixa de entrada                 |");
            System.out.println("|.4 Caixa de saída                   |");
            System.out.println("|.5 Encerrar sessão                  |");
            System.out.println("|                                    |");
            System.out.println("+------------------------------------+");
            System.out.print("|Digite a opção: ");

            try {
                opcao = entrada.nextInt();
            } catch (Exception e) {
                opcao = -1;
            }

            entrada.nextLine();

            switch (opcao) {
                case 1:
                    enviar(usuario);
                    break;
                case 2:
                    exibirPerfil(usuario);
                    break;
                case 3:
                    caixaEntrada(usuario);
                    break;
                case 4:
                    caixaSaida(usuario);
                    break;
                case 5:
                    break OUTER;
                default:
                    break;
            }

            try {
                System.in.read();
            } catch (Exception e) {
            }
        }
    }

    private void enviar(Usuario usuario) {

        System.out.println("+-----------------------------------+");
        System.out.println("|            Enviar email           |");
        System.out.println("+-----------------------------------+\n");
        System.out.println("| " + usuario.getNome() + "\n");
        System.out.print("|Destinatário: ");
        String email = entrada.nextLine();
        System.out.print("|Assunto: ");
        String assunto = entrada.nextLine();
        System.out.print("|Texto: ");
        String texto = entrada.nextLine();

        Usuario destino = null;

        for (int i = 0; i < indiceUser; i++) {
            if (usuarios[i].getEmail().equals(email)) {
                destino = usuarios[i];
                break;
            }
        }

        if (destino != null) {
            try {
                int valor = usuario.enviarEmail(destino, new Mensagem(assunto, texto));

                if (valor == 0) {
                    Connection conexao = null;
                    PreparedStatement pst = null;
                    ResultSet rs = null;

                    conexao = ModuloConexao.conector();

                    String sql = "INSERT INTO email (remetente, destinatario, assunto, texto) VALUES (?,?,?,?);";

                    pst = conexao.prepareStatement(sql);
                    pst.setString(1, usuario.getEmail());
                    pst.setString(2, email);
                    pst.setString(3, assunto);
                    pst.setString(4, texto);
                    pst.executeUpdate();

                    conexao.close();
                    System.out.println("\nOk! A mensagem foi enviada!");
                } else {
                    System.out.println("\nErro. A mensagem não pôde ser enviada porque a sua caixa de saída ou a caixa de entrada do destinatário está(ão) cheia(s).");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("\nErro! Destinatário não encontrado!");
        }
    }

    private void caixaEntrada(Usuario usuario) {

        System.out.println("+----------------------------------------+");
        System.out.println("|            Caixa de entrada            |");
        System.out.println("+----------------------------------------+\n");
        System.out.println("| " + usuario.getNome() + "\n");

        Mensagem[] caixaEntrada = usuario.getCaixaEntrada();

        for (int i = 0; i < caixaEntrada.length; i++) {
            System.out.println("| Assunto: " + caixaEntrada[i].getAssunto());
            System.out.println("| Texto: " + caixaEntrada[i].getTexto());
            System.out.println("| Remetente: " + caixaEntrada[i].getRemetente().getEmail() + "\n");
        }
        System.out.println("+----------------------------------------+");
    }

    private void caixaSaida(Usuario usuario) {

        System.out.println("+----------------------------------------+");
        System.out.println("|             Caixa de saída             |");
        System.out.println("+----------------------------------------+\n");
        System.out.println("| " + usuario.getNome() + "\n");

        Mensagem[] caixaSaida = usuario.getCaixaSaida();

        for (int i = 0; i < caixaSaida.length; i++) {
            System.out.println("| Assunto: " + caixaSaida[i].getAssunto());
            System.out.println("| Texto: " + caixaSaida[i].getTexto());
            System.out.println("| Destinatário: " + caixaSaida[i].getDestino().getEmail() + "\n");
        }
        System.out.println("+----------------------------------------+");
    }

    private void exibirPerfil(Usuario usuario) {

        System.out.println("+----------------------------------------+");
        System.out.println("|               Ver perfil               |");
        System.out.println("+----------------------------------------+\n");
        System.out.println("| Nome: " + usuario.getNome());
        System.out.println("| Email: " + usuario.getEmail());
        System.out.println("| Telefone: " + usuario.getTelefone() + "\n");
        System.out.println("+----------------------------------------+");
    }
}
