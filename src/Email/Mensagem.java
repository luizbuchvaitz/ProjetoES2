
package Email;

public class Mensagem {
    
    private String assunto;
    private String texto;
    private Usuario remetente;
    private Usuario destino;
    
    public Mensagem (String assunto, String texto) {
        setAssunto(assunto);
        setTexto(texto);
    }

    public String getAssunto () {
        return assunto;
    }

    public void setAssunto (String assunto) {
        this.assunto = assunto;
    }

    public String getTexto () {
        return texto;
    }

    public void setTexto (String texto) {
        this.texto = texto;
    }

    public Usuario getRemetente () {
        return remetente;
    }

    public void setRemetente (Usuario remetente) {
        this.remetente = remetente;
    }

    public Usuario getDestino () {
        return destino;
    }

    public void setDestino (Usuario destino) {
        this.destino = destino;
    }
    
}
