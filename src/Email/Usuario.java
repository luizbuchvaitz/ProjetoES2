package Email;

public class Usuario {

    private String nome;
    private String email;
    private String senha;
    private String telefone;

    private final int maxCaixa = 1000;
    private int indiceCxEntrada;
    private int indiceCxSaida;

    private Mensagem[] caixaEntrada = new Mensagem[maxCaixa];
    private Mensagem[] caixaSaida = new Mensagem[maxCaixa];

    public Usuario(String nome, String email, String senha, String telefone) {
        setNome(nome);
        setEmail(email);
        setSenha(senha);
        setTelefone(telefone);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Mensagem[] getCaixaEntrada() {

        Mensagem[] cxEntradaAtual = new Mensagem[indiceCxEntrada];

        for (int i = 0; i < indiceCxEntrada; i++) {
            cxEntradaAtual[i] = caixaEntrada[i];
        }
        return cxEntradaAtual;
    }

    public void setCaixaEntrada(Mensagem[] caixaEntrada) {
        this.caixaEntrada = caixaEntrada;
    }

    public Mensagem[] getCaixaSaida() {

        Mensagem[] cxSaidaAtual = new Mensagem[indiceCxSaida];

        for (int i = 0; i < indiceCxSaida; i++) {
            cxSaidaAtual[i] = caixaSaida[i];
        }
        return cxSaidaAtual;
    }

    public void setCaixaSaida(Mensagem[] caixaSaida) {
        this.caixaSaida = caixaSaida;
    }

    public int enviarEmail(Usuario destino, Mensagem mensagem) {

        if (indiceCxSaida < maxCaixa && destino.indiceCxEntrada < destino.maxCaixa) {

            caixaSaida[indiceCxSaida] = mensagem;
            indiceCxSaida++;

            destino.caixaEntrada[indiceCxEntrada] = mensagem;
            destino.indiceCxEntrada++;

            mensagem.setRemetente(this);
            mensagem.setDestino(destino);

            return 0;

        } else {
            return -1;
        }
    }
}
